$("#links").change(function () {
    parseData();
});

$("#links_form_submit_btn").click(function (e) {
    e.preventDefault(); // avoid to execute the actual submit of the form.
    startLikes();

});

function parseData() {
    if ($("#links").val() === '') {
        alert('Please enter links');
        return false;
    }

    const data = $("#links").val().split('\n');
    const linksArray = [];
    data.forEach(function (single_line) {
        const string = single_line, substring = "https";
        if (string.includes(substring)) {

            let link = single_line.split('https://')[1];
            let substr = 'www.';
            if (!link.includes(substr)) {
                link = 'www.' + link;
            }

            const lastChar = link.substr(-1); // Selects the last character
            if (lastChar != '/') {         // If the last character is not a slash
                link = link + '/';            // Append a slash to it.
            }
            linksArray.push('https://' + link);

        }
    });

    updateProgress(linksArray.length, 0);

    return linksArray;
}

function updateProgress(total_links, done_links) {
    $('#total_links').text(total_links);
    $('#done_links').text(done_links);

    return Promise.resolve(1);
}

async function startLikes() {
    const links = chunkArray(parseData(), 20);
    for (const urls of links) {
        await updateUrl(urls);
    }

    return Promise.resolve(1);
}

/**
 * Returns an array with arrays of the given size.
 *
 * @param urlsArray {Array} array to split
 * @param chunkSize {Integer} Size of every group
 */
function chunkArray(urlsArray, chunkSize) {
    let arrayLength = urlsArray.length;
    let tempArray = [];
    for (let index = 0; index < arrayLength; index += chunkSize) {
        tempArray.push(urlsArray.slice(index, index + chunkSize));
    }

    return tempArray;
}


function chromeTabsExecuteScript(tabId, options) {
    return new Promise((resolve) => {
        chrome.tabs.executeScript(tabId, options, () => {
            resolve();
        });
    });
}

function chromeTabsCreate(options) {
    return new Promise((resolve) => {
        chrome.tabs.create(options, (tab) => {
            resolve(tab);
        });
    });
}

function chromeTabsRemove(tabId) {
    return new Promise((resolve) => {
        chrome.tabs.remove(tabId, () => {
            resolve();
        });
    });
}


function sleepAsync(seconds) {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve();
        }, seconds * 1000); // transform to seconds
    });
}

/**
 *
 * @param urls
 * @returns {Promise<number>}
 */
async function updateUrl(urls) {
    const tabIds = [];
    const total = parseInt($('#total_links').text());
    for (const url of urls) {
        const tab = await chromeTabsCreate({url: url, active: false});
        tabIds.push(tab.id);
        await sleepAsync(1); // hope will fix the last links that are opened and closed immediately

        await chromeTabsExecuteScript(tab.id, {
            code: 'document.getElementsByClassName(\'wpO6b\')[0].click();',
        });
        const updated = parseInt($('#done_links').text()) + 1;
        await updateProgress(total, updated);

        await chromeTabsExecuteScript(tab.id, {
            code: 'document.getElementsByClassName(\'wpO6b\')[0].click();',
        });
    }
    await sleepAsync(1); // hope will fix the last links that are opened and closed immediately
    for (const id of tabIds) {
        await chromeTabsRemove(id);
    }
    await sleepAsync(10);

    return Promise.resolve(1);
}
